/*
 * @Author: Junlang
 * @Date: 2021-07-22 18:39:51
 * @LastEditTime: 2021-07-23 16:20:59
 * @LastEditors: Junlang
 * @FilePath: /openscore/web/src/Conf.js
 */
// Copyright 2021 The casbin Authors. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

export const DefaultProgramName = "talent2021";

export const AuthConfig = {
  serverUrl: "http://localhost:8000",
  clientId: "70e1935bf72eb360d0fa",
  appName: "application_1",
  organizationName: "built-in",
};
