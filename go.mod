module openscore

go 1.16

require github.com/beego/beego/v2 v2.0.1

require (
	github.com/astaxie/beego v1.12.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/oauth2 v0.0.0-20210514164344-f6687ab2804c
	gopkg.in/yaml.v2 v2.3.0 // indirect
	xorm.io/builder v0.3.8
	xorm.io/xorm v1.1.2
)
